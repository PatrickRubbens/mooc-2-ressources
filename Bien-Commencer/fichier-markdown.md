essai
# Partie 1
## Sous-partie 1 : texte
Une phrase sans rien
_Une phrase en italique_
**Une phrase en gras**
[Un lien vers fun-mooc.fr](https://www.fun-mooc.fr)
Une ligne de ```python code ```

## Sous-partie 2 : listes
**Liste à puce**
* item
  * sous-itemm
  * sous-itemm
* item
* item
**Liste numérotée**
1. item1
2. item2
3. item3

## Sous-partie 3 : code
```python
    def foo(n):
        return n**2

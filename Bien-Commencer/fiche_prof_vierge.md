

- Thématique : Quel est le thème principal de l'activité ? On pourra ici donner un lien vers le programme officiel par exemple.

- Notions liées : Seront précisées ici les notions directement rattachées à l'activité ou les sous-thèmes en relation avec le sujet principal.

- Résumé de l’activité : Une description de ce que propose l'activité ; s'il s'agit d'une suite d'exercices ou d'une activité débranchée, s'il s'agit de programmer en utilisant un module particulier de Python etc.

- Objectifs : Pourquoi propose-t-on cette activité ? Quelle acquisition de connaissance est visée ? Quelle manipulation favorise le développement d'une compétence ?

- Auteur :

- Durée de l’activité :

- Forme de participation : individuelle ? en binôme ? en groupe ? en autonomie ?

- Matériel nécessaire :

- Préparation :

- Autres références : liens éventuels vers d'autres cours ou activités similaires ou complémentaires

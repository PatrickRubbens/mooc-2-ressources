# Analyse d'une activité - Turtle

- Objectifs: 
  * se familiariser avec le langage Python. 
  * Aborder la notion de boucle et son intéret.
  * Aborder la notion de fonction, et son intérêt.


- Pré-requis à cette activité: aucun

- Durée de l'activité: 

  3 séances (ou 4 ?) suivant le niveau de pratique de la classe et l'hétérogénéïté.

- Exercices cibles: 
  * exercice 12 - 13 - 14: Boucle et fonction pour dessiner un carré
  * exercice 15: Une double boucle imbriquée
  * exercice 17 - 18 -19 - 20: pour aller plus loin. Reprendre et adapter les concepts précédents.


- Description du déroulement de l'activité:
  * Séance 1 : Présentation de Python. En fonction de l'environnement de travail dont les élèves disposent. On va commencer par utiliser la console, faire des calculs, et en profiter pour expliquer l'importance des modules en Python. On en profite pour demander de calculer une racine carré (utile pour exercice 5 !)  => on montre  l'import du module math (avec sqrt ou *). Ensuite, on laisse les élèves avancent en autonomie sur les exercices 2 à 5. On peut imaginer que l'exercice 5 posera quelques problèmes... les élèves penseront-ils à calculer la longueur de la diagonale?
  
  * Séance 2 : On présente l'intérêt d'un script et la sauvegarde. Exercices 6 et 7 pour redémarrer et insister sur la nécessiter de réfléchir un peu avant de se lancer (calcul simple des angles).
 Exercices 8: il faut avoir réfléchi. Comment construire un polygone régulier? les exercices suivants devraient alors être naturels. C'est le moment de présenter la notion de boucle (on peut imaginer que les élèves auront utilser "à fond" le copier coller). Normalement, au collège, les élèves ont vu les boucles "Scratch". On peut donc "dessiner" au tableau le bloc indenté de la "boucle for". Les élèves qui auront compris vont rapidement passer sur les exercices 12 ou 13... on les laisse chercher une solution. Double boucle ?

  * Séance 3 : Exercice 12,13 et 14. C'est l'occasion de présenter la notion de fonction. On laisse les élèves proposer des choses et on en vient à la syntaxe. Puis on laisse en autonomie en aidant ceux qui éprouvent des difficultés.

  * On peut penser que très peu d'élèves seront allés au bout de cette fiche en 3 séances mais tous auront pu aborder les notions données dans les objectifs. On décide d'une séance 4 ou pas, pour terminer ?

- Anticipation des difficultés des élèves:
  * syntaxe et indentation: prendre le temps d'expliquer qu'un message d'erreur doit être lu avant d'appeler le professeur. Montrer sur quelques erreurs volontaires que l'interpréteur donne le type d'erreur et souvent à quel endroit.
  * compréhension de la notion de fonction et surtout de (des) l'argument(s): bien expliquer que c'est l'appel de la fonction qui "déclenche" l'action. Faire l'analogie avec la notion de fonction en mathématiques.
  * comprendre les boucles et éventuellement les boucles imbriquées: difficile. On peut utiliser un mode "pas à pas" suivant l'intépréteur dont on dispose. Ou utiliser un outils en ligne comme l'excellent [Python tutor](https://pythontutor.com/visualize.html#mode=edit).

- Gestion de l'hétérogénéïté:
  * la fiche semble suffisemment riche pour occuper tous les élèves. Les plus autonomes ont de quoi s'occuper et l'exercice 20 constitu un sacré défi pour des débutants!
  * les exercices 6 7 et 8, séance 2, vont sans doute révéler les élèves en difficulté sur des concepts plus "abstraits" et qui vont nécessiter plus d'accompagnement. Il peut être intéressant de créer des binômes à la séance 3 pour que ceux qui ont pris de l'avance apportent leur aide. Mais attention aux "raccorcis" dans les explications. Il faudra s'assurer que tous les élèves ont bien compris les concepts de base.
